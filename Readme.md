# gitlab-ci-container

Container to be used in ci jobs.

Contains:
 - python
 - npm
 - docker
 - git
