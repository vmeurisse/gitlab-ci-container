#! /bin/sh

set -ex

docker build --build-arg _PYTHON_VERSION="$PYTHON_IMAGE" -t "$CI_REGISTRY/vmeurisse/gitlab-ci-container/gitlab-ci-container:$PYTHON_VERSION" -f Containerfile .

if [ "$CI_COMMIT_BRANCH" = "main" ]; then
	docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
	docker push "$CI_REGISTRY/vmeurisse/gitlab-ci-container/gitlab-ci-container:$PYTHON_VERSION"
fi
